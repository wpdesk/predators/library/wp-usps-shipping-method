## [1.8.0] - 2024-10-17
### Added
- value of contents
 
## [1.7.2] - 2024-10-10
### Fixed
- zones checker

## [1.7.1] - 2024-10-10
### Removed
- global package defaults

## [1.7.0] - 2024-06-17
### Added
- REST API Services configuration

## [1.6.0] - 2024-06-17
### Added
- Tracker

## [1.5.0] - 2024-06-12
### Added
- REST API support

## [1.4.0] - 2024-01-22
### Added
- API password

## [1.3.2] - 2023-12-08
### Changed
- Docs URL

## [1.3.1] - 2023-01-19
### Fixed
- shipping zones without locations

## [1.3.0] - 2023-01-03
### Changed
- custom origin handled in wp-woocommerce-shipping

## [1.2.0] - 2022-08-30
### Added
- de_DE translations

## [1.1.1] - 2022-08-16
### Fixed
- pl_PL translations

## [1.1.0] - 2022-07-07
### Added
- rates cache

## [1.0.1] - 2021-10-04
### Fixed
- docs link

## [1.0.0] - 2021-10-04
### Added
- initial version
